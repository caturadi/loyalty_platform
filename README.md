# README

The application is to demonstrate loyalty program practical example.

At it's most basic, the platform offers clients the ability to issue loyalty points to their end users. End users use their points to claim/purchase rewards offered by the client.

The application was build using current version
* Ruby version 3.0.1 and Rails Version 6.1.4

Please use <b>Postgresql Database</b> and setup Database and user as follows
>  Database Name: loyalty <br>
>  username: pis_dev <br>
>  password: pis12345 <br>

<b>Please do:</b> 
1. gem installation: > bundle install 
2. rails  database migration using > rails db:migrate
3. database seeds <b>(it's a must !)</b> using >rails db:seed 


<b><h1>Functional Requirements</h1></b>

<b>A.Point Earning Rules</b>

* Level 1 - For every $100 the end user spends they receive 10 points
* Level 2 - If the end user spends any amount of money in a foreign country they receive 2x the standard points

<b>B. Issuing Rewards Rule</b>

* Level 1

1. If the end user accumulates 100 points in one calendar month they are given a Free Coffee reward 

* Level 2

1. A Free Coffee reward is given to all users during their birthday month
2. A 5% Cash Rebate reward is given to all users who have 10 or more transactions that have an amount > $100
3. A Free Movie Tickets reward is given to new users when their spending is > $1000 within 60 days of their first transaction


<b>C. Loyalty Rules</b>

* Level 1
1. A standard tier customer is an end user who accumulates 0 points
2. A gold tier customer is an end user who accumulates 1000 points
3. A platinum tier customer is an end user who accumulates 5000 points

Logic and executable logic
please run 
> 1. rails server
> 2. visit http://localhost:3000/main/run

You will see
1. How the customer was setup at first and the point balance was also setup as initial balance wih 0 point. See <b>addUser(name,dob)</b>
2. How the customer perform such transaction both in and outside country. See <b>doTransaction(user,ammount,foreiqn)</b>
3. How the customer earning point logic was performed and customer point balance was updated. See <b>doPointEarnings(ammount, foreign)</b> and <b>run_rewards(user)</b>
4. How the customer tiering was applied to customer. See <b>check_tiers(user)</b>

see the output on the browser.
please refer to main controllers and modify the transactions to see different output. See <b>doTransaction(user,ammount,foreiqn)</b>


doTransaction(user,ammount,foreiqn) to save transaction and earn points. Foreqign flag set to true indicates that the transaction is done outside country hence applicable to double the earning points.

Please refresh the browser ro run several transaction and to get more points hence the page will show how the tiering mechanism will works and how the rewards were collected.



