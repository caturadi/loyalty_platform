class MainController < ApplicationController


    def run
    
     
        
        #1 Add New User if not exists
        puts addUser("mela","1985-03-24")        
        
        #2 Get User
        @user = User.find_by(name:"mela")   

        #Perform Transaction
        if !@user.nil?
            #@user perform tarnsaction with value $200 in the country
            puts doTransaction(@user,200,false)
            sleep 1 #sleep to simulate wait for next transaction

            #@user perform tarnsaction with value $400 in outside country
            puts doTransaction(@user,200,true)
            sleep 2  

            #@user perform tarnsaction with value $50 will not earn point
            puts doTransaction(@user,50,false)

            sleep 1 
            #@user perform tarnsaction with value >$101
            puts doTransaction(@user,101,false)

        end

        #Showing on Screen
        @point = Point.find_by(user_id: @user.id)
        @transactions = Transaction.select("transactions.*, coalesce(point_earnings.point, 0) as point")
                                    .joins("left join point_earnings on point_earnings.transaction_id = transactions.id ")
                                    .where(user_id: @user.id).order("transactions.trx_date asc")

        @rewards = RewardCollected.select("reward_date,'isUsed',reward_name,rule_name")
                                    .joins("join rewards on rewards.id = reward_collecteds.reward_id")
                                    .joins("join rules on rules.id = reward_collecteds.rule_id")
                                    .where(user_id:@user.id)
        
        @tiers = CustomerTier.all.map {|x| [x.code,x.tier_name]}.to_h

    end





    private

    #This function is used to add users to database
    def addUser(name,dob)

        if User.find_by(name:name).nil?
            #1 Creating user
            user = User.new
            user.name = name
            user.birth_of_date = dob.to_date
            user.save

            #2 Creating Points Bucket
            bucket = Point.new
            bucket.user_id= user.id 
            bucket.balance = 0
            bucket.save

            return "adding new user: #{name}"
        else
            return "user: #{name} already there"

        end 

    end

    #This function is used to perform transaction
    def doTransaction(user,ammount,foreiqn)

        msg = ''

        #1. Performing transaction
        trx = Transaction.new
        trx.user_id = user.id
        trx.isForeignTrx = foreiqn
        trx.trx_date = DateTime.now
        trx.ammount = ammount
        trx.save
        
        msg = msg+"Doing $#{trx.ammount} transaction at #{trx.trx_date}} "

        #2. Performing earnings point
        getPoint = doPointEarnings(trx.ammount,trx.isForeignTrx)
        
        #3. Saving Point Earns
        if getPoint > 0 

            pe = PointEarning.new
            pe.user_id = user.id
            pe.transaction_id = trx.id
            pe.point = getPoint
            pe.save

            #update balance
            bucket = Point.find_by(user_id:user)
            bucket.balance = bucket.balance + pe.point
            bucket.save

            msg = msg+" and get #{pe.point} points"

        end

        #4. Running rewards rule enggine.
        run_rewards(user)

        #5. Running customer tiering rules.
        check_tiers(user)

        return msg
    
    end

    #This Function is used calculate how many point that customer get for an ammount of transaction that meet the specification
    def doPointEarnings(ammount, foreign)

        getPoint = ((ammount/100).to_i)*10

        if foreign
            getPoint = getPoint*2
        end

        return getPoint
    end

    #This Function is used to run issuing rewards rules
    def run_rewards(user)
            #This logic is assumed executed after transaction
            @rules = Rule.all
            total_trx = Transaction.where(user_id:user.id).count
            total_ammount = Transaction.where(user_id:user.id).sum(:ammount)
            total_trx_above_100 = Transaction.where(user_id:user.id).where("ammount>100").count

            total_point = Point.find_by(user_id:user.id).balance
            total_point_current_month = PointEarning.select("sum(point) as total").joins("join transactions on transactions.id = point_earnings.transaction_id")
                                                    .where(user_id:user.id)
                                                    .where("trx_date >= '#{DateTime.now.at_beginning_of_month}' and trx_date<= '#{DateTime.now}'").take.total
            first_date_trx = Transaction.where(user_id:user.id).limit(1).order("trx_date asc").take

            #Total Ammount from 1stdate till 6 month
            total_ammount_fsdt = Transaction.where(user_id:user.id).where("trx_date>= '#{first_date_trx.trx_date}' and trx_date <= '#{(first_date_trx.trx_date.to_datetime)+60}' ").sum(:ammount)
            

            puts "Tota Trx: #{total_trx}"
            puts "Tota Ammount: #{total_ammount}"
            puts "Tota Point All Periods: #{total_point}"
            puts "Tota Point Current Month #{total_point_current_month}"
            puts "Tota Ammount from 1st date + 60 days: #{total_ammount_fsdt}"



            @rules.each do |rule|
                    reward= RewardRule.find_by(rule_id:rule.id)

                    if rule.birthdate

                        if user.birth_of_date.strftime('%m-%d') == Date.today.strftime('%m-%d') 
                            if RewardCollected.where(user_id:user.id, reward_id:reward.reward_id,reward_date:Date.today,rule_id:rule.id).count <= 0
                                rc = RewardCollected.new
                                rc.user_id = user.id
                                rc.reward_date = Date.today
                                rc.rule_id = rule.id
                                rc.reward_id = reward.reward_id
                                rc.save
                            end
                        end
                    end

                    if !rule.birthdate and total_trx > rule.target_num_trx and total_ammount > rule.target_total_ammount and total_point_current_month > rule.target_point and rule.period == "1_CALENDAR_MONTH"

                        if RewardCollected.where(user_id:user.id, reward_id:reward.reward_id,rule_id:rule.id).where("date_part('month',reward_date) = #{Date.today.month}").count <= 0
                            rc = RewardCollected.new
                            rc.user_id = user.id
                            rc.reward_date = Date.today
                            rc.reward_id = reward.reward_id
                            rc.rule_id = rule.id
                            rc.save
                        end
                           
                   
                    end

                    if !rule.birthdate and total_trx_above_100 >= rule.target_num_trx and total_ammount > rule.target_total_ammount and total_point > rule.target_point and rule.period == "-"


                        if RewardCollected.where(user_id:user.id, reward_id:reward.reward_id,rule_id:rule.id).count <= 0
                            rc = RewardCollected.new
                            rc.user_id = user.id
                            rc.reward_date = Date.today
                            rc.reward_id = reward.reward_id
                            rc.rule_id = rule.id
                            rc.save
                        end
                           
                   
                    end

                    if !rule.birthdate and total_trx > rule.target_num_trx and total_ammount_fsdt > rule.target_total_ammount and total_point_current_month > rule.target_point and rule.period == "FIRST_TRANSACTION"


                        if RewardCollected.where(user_id:user.id, reward_id:reward.reward_id,rule_id:rule.id).count <= 0
                            rc = RewardCollected.new
                            rc.user_id = user.id
                            rc.reward_date = Date.today
                            rc.reward_id = reward.reward_id
                            rc.rule_id = rule.id
                            rc.save
                        end
                           
                   
                    end
            
            end
    
    
    end

    #This function is used to check and to set whether customer point balance is enough to upgrade to the next tier
    def check_tiers(user)

        tiers = CustomerTier.all
        point = Point.find_by(user_id:user.id)

        tiers.each do |tier|
            
            puts "point balance #{point.balance}"
            puts "code: #{tier.code} min_point #{tier.min_point}"
            if point.balance >= tier.min_point and user.tier != tier.code
                user.tier = tier.code
                user.save
            end

        end
        

    end



end
