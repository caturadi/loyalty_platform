# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_09_12_104837) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "customer_tiers", force: :cascade do |t|
    t.integer "code", null: false
    t.string "tier_name"
    t.integer "min_point", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "point_earnings", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "transaction_id", null: false
    t.integer "point"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["transaction_id"], name: "index_point_earnings_on_transaction_id"
    t.index ["user_id"], name: "index_point_earnings_on_user_id"
  end

  create_table "points", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.integer "balance"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_points_on_user_id"
  end

  create_table "reward_collecteds", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.date "reward_date", null: false
    t.bigint "reward_id", null: false
    t.boolean "isUsed", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "rule_id", default: 0
    t.index ["reward_id"], name: "index_reward_collecteds_on_reward_id"
    t.index ["user_id"], name: "index_reward_collecteds_on_user_id"
  end

  create_table "reward_rules", force: :cascade do |t|
    t.bigint "rule_id", null: false
    t.bigint "reward_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["reward_id"], name: "index_reward_rules_on_reward_id"
    t.index ["rule_id"], name: "index_reward_rules_on_rule_id"
  end

  create_table "rewards", force: :cascade do |t|
    t.string "reward_name"
    t.float "cashback_value"
    t.float "discount_value"
    t.string "reward_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "rules", force: :cascade do |t|
    t.string "rule_name"
    t.boolean "birthdate", default: false
    t.integer "target_point", default: 0
    t.integer "target_num_trx", default: 0
    t.integer "target_total_ammount", default: 0
    t.string "period"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.boolean "isForeignTrx", default: false
    t.datetime "trx_date", null: false
    t.float "ammount", default: 0.0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_transactions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.date "birth_of_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "tier", default: 0
  end

  add_foreign_key "point_earnings", "transactions"
  add_foreign_key "point_earnings", "users"
  add_foreign_key "points", "users"
  add_foreign_key "reward_collecteds", "rewards"
  add_foreign_key "reward_collecteds", "users"
  add_foreign_key "reward_rules", "rewards"
  add_foreign_key "reward_rules", "rules"
  add_foreign_key "transactions", "users"
end
