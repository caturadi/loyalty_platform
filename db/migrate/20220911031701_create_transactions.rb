class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.belongs_to :user, index: true, foreign_key: "user_id", null: false
      t.boolean "isForeignTrx", default: false
      t.datetime "trx_date", null: false
      t.float "ammount", default:0, null: false
      t.timestamps
    end
  end
end
