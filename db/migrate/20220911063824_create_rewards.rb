class CreateRewards < ActiveRecord::Migration[6.1]
  def change
    create_table :rewards do |t|
      t.string :reward_name
      t.float :cashback_value
      t.float :discount_value
      t.string :type
      t.timestamps
    end
  end
end
