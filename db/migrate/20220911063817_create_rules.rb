class CreateRules < ActiveRecord::Migration[6.1]
  def change
    create_table :rules do |t|
      t.string :rule_name
      t.boolean :birthdate, default:false
      t.integer :target_point, default:0
      t.integer :target_num_trx, default:0
      t.integer :target_total_ammount, default:0
      t.string :period
      t.timestamps
    end
  end
end
