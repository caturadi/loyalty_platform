class CreatePointEarnings < ActiveRecord::Migration[6.1]
  def change
    create_table :point_earnings do |t|
      t.belongs_to :user, index: true, foreign_key: "user_id", null: false
      t.belongs_to :transaction, index: true, foreign_key: "transaction_id", null: false
      t.integer :point
      t.timestamps
    end
  end
end
