class CreateRewardRules < ActiveRecord::Migration[6.1]
  def change
    create_table :reward_rules do |t|
      t.belongs_to :rule, index: true, foreign_key: "rule_id", null: false
      t.belongs_to :reward, index: true, foreign_key: "reward_id", null: false
      t.timestamps
    end
  end
end
