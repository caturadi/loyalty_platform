class CreateRewardCollecteds < ActiveRecord::Migration[6.1]
  def change
    create_table :reward_collecteds do |t|
      t.belongs_to :user, index: true, foreign_key: "user_id", null: false
      t.datetime "reward_date", null: false
      t.belongs_to :reward, index: true, foreign_key: "reward_id", null: false
      t.boolean :isUsed, default:false
      t.timestamps
    end
  end
end
