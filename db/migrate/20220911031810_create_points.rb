class CreatePoints < ActiveRecord::Migration[6.1]
  def change
    create_table :points do |t|
      t.belongs_to :user, index: true, foreign_key: "user_id", null: false
      t.integer :balance
      t.timestamps
    end
  end
end
