class ChangeCol < ActiveRecord::Migration[6.1]
  def change
    rename_column :rewards, :type, :reward_type
  end
end
