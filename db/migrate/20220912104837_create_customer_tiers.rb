class CreateCustomerTiers < ActiveRecord::Migration[6.1]
  def change
    create_table :customer_tiers do |t|
      t.integer "code",null:false
      t.string "tier_name"
      t.integer "min_point", default:0
      t.timestamps
    end
  end
end
