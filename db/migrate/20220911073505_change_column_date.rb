class ChangeColumnDate < ActiveRecord::Migration[6.1]
  def change
    change_column :reward_collecteds, :reward_date, :date
  end
end
