# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Rule.create(rule_name:"achieve targeted point",birthdate:false,target_point:100,target_num_trx:0,target_total_ammount:0,period:"1_CALENDAR_MONTH")
Rule.create(rule_name:"birtdate rule",birthdate:true,target_point:0,target_num_trx:0,target_total_ammount:0,period:"-")
Rule.create(rule_name:"achieve targeted number and value transaction",birthdate:false,target_point:0,target_num_trx:10,target_total_ammount:100,period:"-")
Rule.create(rule_name:"achieve targeted value within period",birthdate:false,target_point:0,target_num_trx:0,target_total_ammount:1000,period:"FIRST_TRANSACTION")

Reward.create(reward_name:"Free Coffee",cashback_value:0,discount_value:0,reward_type:"voucher")
Reward.create(reward_name:"Cash Rebate Reward 5%",cashback_value:0,discount_value:0.05,reward_type:"deduction")
Reward.create(reward_name:"Free Movie Ticket",cashback_value:0,discount_value:0,reward_type:"deduction")

RewardRule.create(rule_id:1,reward_id:1)
RewardRule.create(rule_id:2,reward_id:1)
RewardRule.create(rule_id:3,reward_id:2)
RewardRule.create(rule_id:4,reward_id:3)

CustomerTier.create(code:1,tier_name:"Standard",min_point:0)
CustomerTier.create(code:2,tier_name:"Gold",min_point:1000)
CustomerTier.create(code:3,tier_name:"Platinum",min_point:5000)



